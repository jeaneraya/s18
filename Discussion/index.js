// Function able to receive data without the use of global variables or prompt()


//name - is a parameter
// A parameter is a variable/container that exist only in out function and is used to store information that is provided to a function when it is called/invoked

function printName(name){
	console.log("My name is " + name);
};

//DAta passed into a function invocation can be received by the function

//This is what we call an argument
printName("Jungkook");
printName("Jean");


// Data passed into the function through function invocation is called arguments

// The argument is then stored within a container is called parameter
function printMyAge(age){
	console.log("I am " + age)
};

printMyAge(25);
printMyAge();

//check divisibility reusably using a function with arguments and parameter

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleby8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleby8);
};

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);

/*function faveSuperhero(superhero){
	console.log("Your favorite superhero is: " + superhero);
};
let superheroName = prompt ("Who is your favorite superhero?");
faveSuperhero(superheroName);

function getNum(num){
	let isDivisibleby2 = num % 2;
	let isNumEven = isDivisibleby2 === 0;

	console.log("The number you enter is: " + num);
	console.log("Is the entered number an even number?")
	console.log(isNumEven);
	
}

let input = prompt("Enter a number");
getNum(input);*/

function printFullName(firstName,middleInitial,lastName){
	console.log(firstName + " " + middleInitial + " " + lastName);
};

printFullName("Jean Victoria","D.", "Eraya");

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an error or changes the behavior of the function
*/

printFullName("Jean","Eraya"); // Jean Eraya undefined

// use variable as arguments
let fname = "Trisha"
let mname = "B."
let lname = "Marites"

printFullName(fname,mname,lname);

function faveSongs(s1,s2,s3,s4,s5){
	console.log("These are my favorite songs: ");
	console.log(s1);
	console.log(s2);
	console.log(s3);
	console.log(s4);
	console.log(s5);
};

faveSongs("Wait a Minute","Unforgiven2","Favorite Mistake","Weak","Incomplete");

//Return Statement
//Currentyly or so far, our function aree able to display data in our console
//However, our function cannot yet return values. Function are able to return values which can be saved into a variable using the return statement/keyword

let fullName = printFullName("Jean","D","Eraya");
console.log(fullName);//undefined

function returnFullName(firstName,middleInitial,lastName){
	return firstName + " " + middleInitial + " " + lastName;
};

fullName = returnFullName("Carl John","D","Violanda");
//printFullName(); return undefined because the function does not have return statement
//returnFullName();

console.log(fullName);

console.log(fullName + " is my Grandfather.");

function returnPhiAddress(city){
	return city + " ,Philippines";
};

//return - return values from a function which we can save in a variable
let myFullAddress = returnPhiAddress("Kidapawan");
console.log(myFullAddress);

//return true if number is divisible by , else returns false
function checkDivisibilityBt4(number){
	let remainder = number % 4;
	let isDivisibleby4 = remainder === 0;

	//returns either true or false
	//Not only can you return row values/data, you can also directly return a value

	//console.log(isDivisibleby4);
	return isDivisibleby4;
	// return keyword not only allows us to return value but also ends the process of the function
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBt4(4);
let num14isDivisibleBy4 = checkDivisibilityBt4(14);

/*console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);*/

checkDivisibilityBt4(16);


function sum(num1,num2){
	return num1 + num2;
};

let sumOfTwoNum = sum(2,3);
console.log(sumOfTwoNum);

function prodOfTwoNum(num1,num2){
	return num1 + num2;
};

let product = sum(2,3);
console.log(product);


function areaOfCircle(radius){
	return 3.14 * radius**2;
};

let circleArea= areaOfCircle(2);
console.log(circleArea);

function checkIfPassing(score,totalScore){
	let passingPercentage = 75;

	let yourScore =  (score / totalScore ) * 100;
	let isScorePassed = yourScore > passingPercentage;
	return isScorePassed;
};

let isPassingScore = checkIfPassing(45,50);
console.log(isPassingScore);